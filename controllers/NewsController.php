<?php

namespace app\controllers;

use app\models\prevNews;
use Yii;
use yii\web\Controller;
use app\models\detailNews;


class NewsController extends Controller {

    public function actionIndex()
    {
        $query = prevNews::find();
        $news = $query->orderBy('id')->limit(50)->all();
        return $this->render('index', [
            'news' => $news,
        ]);
    }

    public function actionNewsDetailJson()
    {
        $news = new detailNews();
        $detail_news = $news->ajaxNewsDetail($_POST['data']);
        echo json_encode($detail_news);
    }

    public function actionMoreNews() { //TODO:дописать аякс подгрузку новостей (http://www.codeharmony.ru/materials/136)
        $news = new prevNews();
        $news_list = $news->getNews();
    }

}
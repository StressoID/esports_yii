<?php

namespace app\controllers;

use app\models\RegForm;
use Yii;
use yii\web\Controller;
use app\models\User;

class RegistrationController extends Controller
{
    public $layout = 'login_register';



    public function actionIndex()
    {


        $model = new RegForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($user = $model->regUser()) {
                if (Yii::$app->getUser()->login($user)) {
                    //return $this->goHome();
                }
            } else {
                Yii::$app->session->setFlash('error', 'Регистрация не прошла');
                Yii::error('Регистрация не прошла');
                return $this->refresh();
            }
        }
        return $this->render('index', [
            'model' => $model,
        ]);

    }

}

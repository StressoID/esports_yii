<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegForm extends model
{

    public $username;
    public $password;
    public $email;
    public $first_name;
    public $last_name;
    public $male;
    public $birthday;

    public function rules()
    {
        return [
            [['username','email', 'password', 'male', 'first_name', 'last_name', 'birthday'], 'filter', 'filter' => 'trim'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 255],
            // username are both required
            [['username'], 'required', 'message' => 'Имя пользователя не корректно'],
            // password are both required
            [['password'], 'required', 'message' => 'Пароль не корректен'],
            // password is validated by validatePassword()
            //['password', 'validatePassword'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'Эта почта уже занята'],
            ['email', 'required', 'message' => 'E-mail не корректен']
        ];
    }

    public function regUser() {  
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPasswordHash($this->password);
        $user->generateAuthKey();
        $user->generateAccessToken();
        $user->male = $this->male;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->birthday = date('Y-m-d', strtotime($this->birthday));

        return $user->save() ? $user : null;
    }
}
<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class detailNews extends ActiveRecord
{
    //обработка ajax запроса на выгрузку детальной новости
    private $post_data;
    public function ajaxNewsDetail($data)
    {
        $this->post_data = $data;
        if ($this->validate()) {
            $query = detailNews::find()
                ->where(['code' => $data])
                ->one();
            $news = array(
                'id' => $query['id'],
                'name' => $query['title'],
                'text' => $query['text']
            );

            return $news;
        } else {
            return false;
        }
    }
}

<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class prevNews extends ActiveRecord
{

    /*выгрузка новостей на страницу
    при первом вызове функции(загрузка страницы) отправляется 50 новостей
    далее аяксом дергается еще 50 и так по кругу*/
    private $news;
    public function getNews($id) {
        $query = prevNews::find();
        $this->news = $query->orderBy('id')->limit($id)->offset(50)->all();

        return $this->news;
    }
}
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;


AppAsset::register($this);?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="/esports/basic/web/images/favicon.ico" type="image/x-icon">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="wrapper">
    <div id="header">
        <div id="inner_header">
            <div id="logo">
                <a href="/"><img width="150px" src="/esports/basic/web/images/logo_gray_small.png"></a>
            </div>
            <div id="search_menu">
                <div id="top_menu">
                    <ul class="top_menu_ul">
                        <li><a href="/">Главная</a></li>
                        <li><a href="/news/">Новости</a></li>
                        <li><a href="/login/">Вход</a></li>
                    </ul>
                </div>
                <div id="search">
                    <div id="search_icon">
                        <img class="search_icon" src="/esports/basic/web/images/search.png">
                    </div>
                    <div class="search_input_div">
                        <input class="search_input" type="text" name="search" placeholder="Поиск">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="login_content">
        <?=$content?>
    </div>
    <div id="footer">
        <div id="copyright">
            <span class="copy">Copyright by myesports.ru 2015</span>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

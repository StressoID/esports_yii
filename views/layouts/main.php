<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?if (strpos($_SERVER["REQUEST_URI"], 'news')) {?>
    <div id="layout">
        <div style="clear: both"></div>
    </div>

    <div id="popup">
        <div id="popup_content">
            <div class="news_element">
                <h3>

                </h3>
                <p class="news_prev_text">

                </p>
                <img style="display:none" src="">
                <div class="hide_news">
                    <a href="#">Закрыть</a>
                    <p class="news_date">

                     </p>
                </div>

            </div>
        </div>
        <div style="clear: both"></div>
    </div>
<?}?>
<?php $this->beginBody() ?>
    <div id="wrapper">
        <div id="header">
            <div id="inner_header">
                <div id="logo">
                    <a href="<?= Yii::$app->homeUrl;?>"><img width="150px" src="images/logo_gray_small.png"></a>
                </div>


                <div id="search_menu">
                    <div id="top_menu">
                        <ul class="top_menu_ul">
                            <li><a href="/">Главная</a></li>
                            <li><a href="/news">Новости</a></li>

                            <?if (Yii::$app->user->isGuest) {?>
                                <li><a href="/login">Вход</a></li>
                            <?} else {?>
                                <li><a data-method="post" href="/site/logout">Выход</a></li>
                            <?}?>
                        </ul>
                    </div>
                    <div id="search">
                        <div class="search_input_div">
                            <input class="search_input" type="text" name="search" placeholder="Поиск">
                        </div>
                        <div id="search_icon">
                            <img class="search_icon" src="images/search.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="full_content">
            <?if (!Yii::$app->user->isGuest) {?>
                <div id="left_menu_out">
                    <div id="left_menu">
                        <div class="left_menu_elem <?if ($_SERVER['REQUEST_URI'] == '/'){?>active<?}?>">
                            <a class="left_menu_link" href="/"><img src="images/profile.png" width="50px" alt="Профиль"></a>
                        </div>
                        <div class="left_menu_elem  <?if ($_SERVER['REQUEST_URI'] == '/news/'){?>active<?}?>">
                            <a class="left_menu_link" href="#"><img src="images/news.png" width="50px" alt="Новости"></a>
                        </div>
                        <div class="left_menu_elem">
                            <a class="left_menu_link" href="#"><img src="images/bets.png" width="50px" alt="Ставки"></a>
                        </div>
                        <div class="left_menu_elem">
                            <a class="left_menu_link" href="#"><img src="images/learn.png" width="50px" alt="Обучение"></a>
                        </div>
                        <div class="left_menu_elem">
                            <a class="left_menu_link" href="#"><img src="images/config.png" width="50px" alt="Настройки"></a>
                        </div>
                        <div class="left_menu_elem">
                            <a class="left_menu_link" href="#"><img src="images/exit.png" width="50px" alt="Выход"></a>
                        </div>
                    </div>
                </div>
            <?}?>
            <div id="content">
                <?= $content ?>
            </div>
        </div>
        <?if (!strpos($_SERVER["REQUEST_URI"], 'news')) {?>
            <div id="footer">
                <div id="copyright">
                    <span class="copy">Copyright by myesports.ru 2015</span>
                </div>
            </div>
        <?}?>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

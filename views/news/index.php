<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Новости';

?>
<div id="breadcrumbs">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<pre><?var_dump(Yii::$app->user->id)?></pre>
<div id="news_list">
    <ul>

        <?foreach ($news as $new) {?>
            <li>
                <div class="news_element">
                    <h3>
                        <?=$new['name']?>
                    </h3>
                    <p class="news_prev_text">
                        <?=$new['text']?>
                    </p>
                    <div class="news_img"><img src="<?=$new['img']?>"></div>
                    <div class="show_news">
                        <a data-code="<?=$new['code']?>" data-id="<?=$new['id']?>" href="#<?=$new['code']?>">Просмотреть</a>
                    </div>
                    <p class="news_date">
                        <?=$new['date']?>
                    </p>
                </div>

                <div style="clear: both"></div>
            </li>
        <?}?>
    </ul>
</div>

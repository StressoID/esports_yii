<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\RegForm;

$this->title = 'Регистрация';

$this->params['breadcrumbs'][] = $this->title;
?>
<div id="h1_login">
    <h1>Welcome</h1>
</div>
<div id="login_inner_content">
    <div id="h3_login">
        <h3>Регистрация</h3>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'reg-form',
        'options' => ['class' => 'form-horizontal'],
        'action' => '',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <div class="login_inputs">
        <div class="input_type">
            <?= $form->field($model, 'username')->label('Логин') ?>
        </div>
        <div class="input_type">
            <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>
        </div>
        <div class="input_type">
            <?= $form->field($model, 'email')->label('E-Mail') ?>
        </div>
        <div class="input_type">
            <?= $form->field($model, 'first_name')->label('Имя') ?>
        </div>
        <div class="input_type">
            <?= $form->field($model, 'last_name')->label('Фамилия') ?>
        </div>
        <div class="input_type">
            <?= $form->field($model, 'birthday')->label('Дата рождения')->textInput(['placeholder' => '00.00.0000']) ?>
        </div>
        <div class="input_type">
            <?= $form->field($model, 'male')->label('Пол')->
            dropDownList(
                ['male' => 'Мужской', 'female' => 'Женский',],
                ['prompt'=>'Выберите пол']
            ); ?>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'reg-button']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div style="clear: both;"></div>

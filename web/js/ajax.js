$(document).ready(function () {
    var code = window.location.hash.substr(1);
    if (code != ''){
        $.ajax({
            dataType: 'json',
            url: '/news/news-detail-json',
            type: 'POST',
            data: {data: code},
            success: function (data) {
                $('#popup_content h3').text(data.name);
                $('#popup_content .news_prev_text').html(data.text);
                if (data.image != '') {
                    $('#popup_content img').attr('src', data.image);
                    $('#popup_content img').show();
                }
            }
        });
    } else {
        $(window).bind('hashchange', function () {
            var code = window.location.hash.substr(1);
            $.ajax({
                dataType: 'json',
                url: '/news/news-detail-json',
                type: 'POST',
                data: {data: code},
                success: function (data) {
                    $('#popup_content h3').text(data.name);
                    $('#popup_content .news_prev_text').html(data.text);
                    if (data.image != '') {
                        console.log('if');
                        $('#popup_content img').attr('src', data.image);
                        $('#popup_content img').show();
                    }
                }
            });
        });
    }
});
$(document).ready(function(){
    var code = window.location.hash.substr(1);
    if (code != ''){
        $('#layout').show();
        $('#popup').show();
        $('#popup_content').show();
        var oBlock = document.getElementById('popup_content');
        $('#news_list').css('height',oBlock.offsetHeight);
        $('#news_list').css('overflow','hidden');
    } else {
        $('.show_news a').click(function() {
            $('#layout').show();
            $('#popup').show();
            $('#popup_content').show();
            var oBlock = document.getElementById('popup_content');
            $('#news_list').css('height',oBlock.offsetHeight);
            $('#news_list').css('overflow','hidden');
        });
        $('.hide_news a').click(function() {
            $('#popup_content').hide();
            $('#popup').hide();
            $('#layout').hide();
            $('#news_list').css('overflow','visible');
            $('#news_list').css('height','100%');
        });

        $('#layout').click(function() {
            $('#popup_content').hide();
            $('#popup').hide();
            $('#layout').hide();
            $('#news_list').css('overflow','visible');
            $('#news_list').css('height','100%');
        });
        /*popup workd end*/
    }


});